/*  Copyright (C) 2015-2017 Andreas Shimokawa, Carsten Pfeiffer, Daniele
    Gobbetti, Lem Dulfo

    This file is part of Gadgetbridge.

    Gadgetbridge is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gadgetbridge is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */
package com.pasid.mibandcardio.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import com.pasid.mibandcardio.GBApplication;
import com.pasid.mibandcardio.R;
import com.pasid.mibandcardio._a._utils.Ivan_Utils;
import com.pasid.mibandcardio.activities.charts.ChartsActivity;
import com.pasid.mibandcardio.devices.DeviceCoordinator;
import com.pasid.mibandcardio.devices.DeviceManager;
import com.pasid.mibandcardio.impl.GBDevice;
import com.pasid.mibandcardio.model.BatteryState;
import com.pasid.mibandcardio.util.DeviceHelper;
import com.pasid.mibandcardio.util.GB;

/**
 * Adapter for displaying GBDevice instances.
 */
public class GBDeviceAdapterv2 extends RecyclerView.Adapter<GBDeviceAdapterv2.ViewHolder> {

    private final Activity activity;
    private final Context context;
    private List<GBDevice> deviceList;
    private int expandedDevicePosition = RecyclerView.NO_POSITION;
    private ViewGroup parent;

    public GBDeviceAdapterv2(Activity activity, Context context, List<GBDevice> deviceList) {
        this.activity = activity;
        this.context = context;
        this.deviceList = deviceList;
    }

    @Override
    public GBDeviceAdapterv2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_itemv2, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final GBDevice device = deviceList.get(position);
        final DeviceCoordinator coordinator = DeviceHelper.getInstance().getCoordinator(device);

        /*
        holder.container.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (device.isInitialized() || device.isConnected()) {
                    showTransientSnackbar(R.string.controlcenter_snackbar_need_longpress);
                } else {
                    showTransientSnackbar(R.string.controlcenter_snackbar_connecting);
                    GBApplication.deviceService().connect(device);
                }
            }
        });

        holder.container.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (device.getState() != GBDevice.State.NOT_CONNECTED) {
                    showTransientSnackbar(R.string.controlcenter_snackbar_disconnecting);
                    GBApplication.deviceService().disconnect();
                }
                return true;
            }
        });*/

        //holder.deviceImageView.setImageResource(R.drawable.level_list_device);
        //level-list does not allow negative values, hence we always add 100 to the key.
        //holder.deviceImageView.setImageLevel(device.getType().getKey() + 100 + (device.isInitialized() ? 100 : 0));

        holder.deviceNameLabel.setText(getUniqueDeviceName(device));

        if (device.isBusy()) {
            holder.deviceStatusLabel.setText(device.getBusyTask());
            holder.busyIndicator.setVisibility(View.VISIBLE);
        } else {
            holder.deviceStatusLabel.setText(device.getStateString());
            //holder.busyIndicator.setVisibility(View.INVISIBLE);
        }

        //begin of action row
        //battery
        holder.batteryStatusBox.setVisibility(View.GONE);
        final short batteryLevel = device.getBatteryLevel();
        if (batteryLevel != GBDevice.BATTERY_UNKNOWN) {

            holder.removeDevice.setVisibility(View.GONE);
            holder.batteryStatusBox.setVisibility(View.VISIBLE);
            holder.connectDisconnect.setText(activity.getString(R.string.tap_to_disconnect));
            holder.connectDisconnect.setEnabled(true);
            holder.showActivityGraphs.setVisibility(View.VISIBLE);

            holder.batteryStatusLabel.setText(device.getBatteryLevel() + "%");
            BatteryState batteryState = device.getBatteryState();
            if (BatteryState.BATTERY_CHARGING.equals(batteryState) ||
                    BatteryState.BATTERY_CHARGING_FULL.equals(batteryState)) {
                holder.batteryIcon.setImageLevel(device.getBatteryLevel() + 100);
            } else {
                holder.batteryIcon.setImageLevel(device.getBatteryLevel());
            }
        }

        //fetch activity data
        //holder.fetchActivityDataBox.setVisibility((device.isInitialized() && coordinator.supportsActivityDataFetching()) ? View.VISIBLE : View.GONE);
        /*

        holder.fetchActivityData.setOnClickListener(new View.OnClickListener()

                                                    {
                                                        @Override
                                                        public void onClick(View v) {
                                                            showTransientSnackbar(R.string.busy_task_fetch_activity_data);
                                                            GBApplication.deviceService().onFetchActivityData();
                                                        }
                                                    }
        );


        //take screenshot
        //holder.takeScreenshotView.setVisibility((device.isInitialized() && coordinator.supportsScreenshots()) ? View.VISIBLE : View.GONE);
        holder.takeScreenshotView.setOnClickListener(new View.OnClickListener()

                                                     {
                                                         @Override
                                                         public void onClick(View v) {
                                                             showTransientSnackbar(R.string.controlcenter_snackbar_requested_screenshot);
                                                             GBApplication.deviceService().onScreenshotReq();
                                                         }
                                                     }
        );

        //manage apps
        //holder.manageAppsView.setVisibility((device.isInitialized() && coordinator.supportsAppsManagement()) ? View.VISIBLE : View.GONE);
        holder.manageAppsView.setOnClickListener(new View.OnClickListener()

                                                 {
                                                     @Override
                                                     public void onClick(View v) {
                                                         DeviceCoordinator coordinator = DeviceHelper.getInstance().getCoordinator(device);
                                                         Class<? extends Activity> appsManagementActivity = coordinator.getAppsManagementActivity();
                                                         if (appsManagementActivity != null) {
                                                             Intent startIntent = new Intent(context, appsManagementActivity);
                                                             startIntent.putExtra(GBDevice.EXTRA_DEVICE, device);
                                                             context.startActivity(startIntent);
                                                         }
                                                     }
                                                 }
        );

        //set alarms
        //holder.setAlarmsView.setVisibility(coordinator.supportsAlarmConfiguration() ? View.VISIBLE : View.GONE);
        holder.setAlarmsView.setOnClickListener(new View.OnClickListener()

                                                {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent startIntent;
                                                        startIntent = new Intent(context, ConfigureAlarms.class);
                                                        context.startActivity(startIntent);
                                                    }
                                                }
        );

        */
        //show graphs
        //holder.showActivityGraphs.setVisibility(coordinator.supportsActivityTracking() ? View.VISIBLE : View.GONE);
        holder.showActivityGraphsButton.setOnClickListener(new View.OnClickListener()

                                                     {
                                                         @Override
                                                         public void onClick(View v) {
                                                             Intent startIntent;
                                                             startIntent = new Intent(context, ChartsActivity.class);
                                                             startIntent.putExtra(GBDevice.EXTRA_DEVICE, device);
                                                             context.startActivity(startIntent);
                                                         }
                                                     }
        );

        //holder.showConfigureOptions.setVisibility(coordinator.supportsActivityTracking() ? View.VISIBLE : View.GONE);
        //holder.configureUserInfo.setOnClickListener(new View.OnClickListener()
        holder.configureOption.setOnClickListener(new View.OnClickListener()

                                                     {
                                                         @Override
                                                         public void onClick(View v) {
                                                             Ivan_Utils.verifyStoragePermissions(activity);
                                                             asksForUserInfo(holder);
                                                             /*Intent formularioDadosUsuario = new Intent(context, FormularioIvan.class);
                                                             context.startActivity(formularioDadosUsuario);
                                                             activity.finish();*/
                                                         }
                                                     }
        );

        ItemWithDetailsAdapter infoAdapter = new ItemWithDetailsAdapter(context, device.getDeviceInfos());
        infoAdapter.setHorizontalAlignment(true);

        readUserInfo(holder);

        holder.connectDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (GB.isBluetoothEnabled()) {

                    holder.connectDisconnect.setEnabled(false);

                    if (device.getState() != GBDevice.State.NOT_CONNECTED) {
                        showTransientSnackbar(R.string.controlcenter_snackbar_disconnecting);
                        GBApplication.deviceService().disconnect();

                        holder.showActivityGraphs.setVisibility(View.GONE);

                        holder.connectDisconnect.setText( activity.getString(R.string.tap_to_connect) );

                        holder.removeDevice.setVisibility(View.VISIBLE);

                        holder.connectDisconnect.setEnabled(true);
                    }
                    else {

                        showTransientSnackbar(R.string.controlcenter_snackbar_connecting);
                        GBApplication.deviceService().connect(device);
                    }


                } else {
                    showTransientSnackbar(R.string.bluetooth_is_disabled_);
                }

                /*

                if (device.getState() != GBDevice.State.NOT_CONNECTED) {
                    showTransientSnackbar(R.string.controlcenter_snackbar_disconnecting);
                    GBApplication.deviceService().disconnect();
                    holder.connectDisconnect.setText( activity.getString(R.string.tap_to_connect) );

                    holder.showActivityGraphs.setVisibility(View.GONE);
                }
                else {
                    if (device.isInitialized() || device.isConnected()) {
                        showTransientSnackbar(R.string.controlcenter_snackbar_need_longpress);
                    } else {
                        showTransientSnackbar(R.string.controlcenter_snackbar_connecting);
                        GBApplication.deviceService().connect(device);
                    }
                } */
            }
        });

        //holder.deviceInfoList.setAdapter(infoAdapter);
        //justifyListViewHeightBasedOnChildren(holder.deviceInfoList);
        //holder.deviceInfoList.setFocusable(false);

        /*
        final boolean detailsShown = position == expandedDevicePosition;
        boolean showInfoIcon = device.hasDeviceInfos() && !device.isBusy();
        //holder.deviceInfoView.setVisibility(showInfoIcon ? View.VISIBLE : View.GONE);
        //holder.deviceInfoBox.setActivated(detailsShown);
        //holder.deviceInfoBox.setVisibility(detailsShown ? View.VISIBLE : View.GONE);
        holder.deviceInfoView.setOnClickListener(new View.OnClickListener() {
                                                     @Override
                                                     public void onClick(View v) {
                                                         expandedDevicePosition = detailsShown ? -1 : position;
                                                         TransitionManager.beginDelayedTransition(parent);
                                                         notifyDataSetChanged();
                                                     }
                                                 }

        );*/

        /*

        //holder.findDevice.setVisibility(device.isInitialized() ? View.VISIBLE : View.GONE);
        holder.findDevice.setOnClickListener(new View.OnClickListener()

                                             {
                                                 @Override
                                                 public void onClick(View v) {
                                                     if (device.getType() == DeviceType.VIBRATISSIMO) {
                                                         Intent startIntent;
                                                         startIntent = new Intent(context, VibrationActivity.class);
                                                         startIntent.putExtra(GBDevice.EXTRA_DEVICE, device);
                                                         context.startActivity(startIntent);
                                                         return;
                                                     }
                                                     GBApplication.deviceService().onFindDevice(true);
                                                     //TODO: extract string resource if we like this solution.
                                                     Snackbar.make(parent, R.string.control_center_find_lost_device, Snackbar.LENGTH_INDEFINITE).setAction("Found it!", new View.OnClickListener() {
                                                         @Override
                                                         public void onClick(View v) {
                                                             GBApplication.deviceService().onFindDevice(false);
                                                         }
                                                     }).setCallback(new Snackbar.Callback() {
                                                         @Override
                                                         public void onDismissed(Snackbar snackbar, int event) {
                                                             GBApplication.deviceService().onFindDevice(false);
                                                             super.onDismissed(snackbar, event);
                                                         }
                                                     }).show();
//                                                     ProgressDialog.show(
//                                                             context,
//                                                             context.getString(R.string.control_center_find_lost_device),
//                                                             context.getString(R.string.control_center_cancel_to_stop_vibration),
//                                                             true, true,
//                                                             new DialogInterface.OnCancelListener() {
//                                                                 @Override
//                                                                 public void onCancel(DialogInterface dialog) {
//                                                                     GBApplication.deviceService().onFindDevice(false);
//                                                                 }
//                                                             });
                                                 }
                                             }

        );*/

        // remove device, hidden under details
        holder.removeDevice.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(context)
                        .setCancelable(true)
                        .setTitle(context.getString(R.string.controlcenter_delete_device_name, device.getName()))
                        .setMessage(R.string.controlcenter_delete_device_dialogmessage)
                        .setPositiveButton(R.string.Delete, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    DeviceCoordinator coordinator = DeviceHelper.getInstance().getCoordinator(device);
                                    if (coordinator != null) {
                                        coordinator.deleteDevice(device);
                                    }
                                    DeviceHelper.getInstance().removeBond(device);
                                    Ivan_Utils.setStepsState(context, "false");
                                } catch (Exception ex) {
                                    //GB.toast(context, "Error deleting device: " + ex.getMessage(), Toast.LENGTH_LONG, GB.ERROR, ex);
                                } finally {
                                    Intent refreshIntent = new Intent(DeviceManager.ACTION_REFRESH_DEVICELIST);
                                    LocalBroadcastManager.getInstance(context).sendBroadcast(refreshIntent);
                                }
                            }
                        })
                        .setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
            }
        });

    }

    private void readUserInfo(final ViewHolder holder) {

        if (Ivan_Utils.getUser(context).length > 2) {
            String username = Ivan_Utils.getUser(context)[0];
            String age = Ivan_Utils.getUser(context)[1];
            String gender = Ivan_Utils.getUser(context)[2];
            holder.userinfo.setText(username);
            holder.userGender.setText(gender);
            holder.userAge.setText(age + " " + activity.getString(R.string.age_postfix));

            holder.showActivityGraphsButton.setEnabled(true);
        }

    }

    private void asksForUserInfo(final ViewHolder holder) {
        //Preparing views
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.activity_formulario_ivan, (ViewGroup) activity.findViewById(R.id.request_monitor_root_parent));
        //layout_root should be the name of the "top-level" layout node in the dialog_layout.xml file.

        final Spinner genderSp = (Spinner) layout.findViewById(R.id.sp_gender);
        final EditText nameEt = (EditText) layout.findViewById(R.id.et_name);
        final EditText ageEt = (EditText) layout.findViewById(R.id.et_age);

        String userData[] = Ivan_Utils.getUser(activity);
        if (userData.length > 2) {
            nameEt.setText(userData[0]);
            ageEt.setText(userData[1]);

            if (userData[2].equalsIgnoreCase(activity.getString(R.string.male_gender) + "\n") || userData[2].equalsIgnoreCase(activity.getString(R.string.male_gender)))
                genderSp.setSelection(0);
            else
                genderSp.setSelection(1);
        }

        final Button okB = (Button) layout.findViewById(R.id.b_save_dt);
        //okB.setVisibility(View.GONE);

        //Initialize spinner
        String[] genders = new String[2];
        genders[0] = activity.getString(R.string.male_gender);
        genders[1] = activity.getString(R.string.female_gender);

        ArrayAdapter<String> spinnerArray = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, genders);
        spinnerArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSp.setAdapter(spinnerArray);
        genderSp.setSelection(0);

        //Building dialog
        AlertDialog.Builder builder = new AlertDialog.Builder( activity );
        builder.setTitle(activity.getString(R.string.user_info));
        builder.setView(layout);
        builder.setCancelable(true);

        final AlertDialog dialog = builder.create();
        okB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String username = nameEt.getText().toString();
                String userAge = ageEt.getText().toString();
                String userGender = (String) genderSp.getSelectedItem();

                if (userAge.isEmpty())
                    ageEt.setError(activity.getString(R.string.ivan_error_age));
                else if (Integer.parseInt(userAge) < 18 || Integer.parseInt(userAge) > 100)
                    ageEt.setError(activity.getString(R.string.ivan_error_age));
                else {
                    if (username.isEmpty())
                        username = activity.getString(R.string.user_info_not_informed);

                    Ivan_Utils.saveUserInformation(activity, username, userAge, userGender);

                    readUserInfo(holder);
                    dialog.dismiss();
                }
            }
        });
        dialog.show();

    }

    @Override
    public int getItemCount() {
        return deviceList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        //CardView container;

        ImageView deviceImageView;
        TextView deviceNameLabel;
        TextView deviceStatusLabel;

        //actions
        LinearLayout batteryStatusBox;
        TextView batteryStatusLabel;
        ImageView batteryIcon;
        LinearLayout fetchActivityDataBox;
        ImageView fetchActivityData;
        ProgressBar busyIndicator;
        ImageView takeScreenshotView;
        ImageView manageAppsView;
        ImageView setAlarmsView;
        //ImageView showActivityGraphs;
        LinearLayout showActivityGraphs, showConfigureOptions;
        Button connectDisconnect;
        Button showActivityGraphsButton, showConfigureOptionsButton, connectDisconnectButton;
        TextView userinfo, userGender, userAge;

        ImageView deviceInfoView;
        //overflow
        //final RelativeLayout deviceInfoBox;
        ListView deviceInfoList;
        ImageView findDevice;
        ImageView configureUserInfo, removeDevice;

        ImageView configureOption;

        ViewHolder(View view) {
            super(view);
            //container = (CardView) view.findViewById(R.id.card_view);

            //deviceImageView = (ImageView) view.findViewById(R.id.device_image);
            deviceNameLabel = (TextView) view.findViewById(R.id.device_name);
            deviceStatusLabel = (TextView) view.findViewById(R.id.device_status);

            //actions
            configureOption = (ImageView) view.findViewById(R.id.device_action_show_configure);
            removeDevice = (ImageView) view.findViewById(R.id.device_action_remove);

            batteryStatusBox = (LinearLayout) view.findViewById(R.id.device_battery_status_box);
            batteryStatusLabel = (TextView) view.findViewById(R.id.battery_status);
            batteryIcon = (ImageView) view.findViewById(R.id.device_battery_status);

            showActivityGraphs = (LinearLayout) view.findViewById(R.id.device_action_show_activity_graphs);
            //showConfigureOptions = (LinearLayout) view.findViewById(R.id.device_action_show_configure);
            connectDisconnect = (Button) view.findViewById(R.id.connect_disconnect_button);
            userinfo = (TextView) view.findViewById(R.id.device_itemv2_userinfo);
            userGender = (TextView) view.findViewById(R.id.device_itemv2_userinfoGender);
            userAge = (TextView) view.findViewById(R.id.device_itemv2_userinfoAge);

            showActivityGraphsButton = (Button) view.findViewById(R.id.device_action_show_activity_graphs_button);
            //showConfigureOptionsButton = (Button) view.findViewById(R.id.device_action_show_configure_button);
            connectDisconnectButton = (Button) view.findViewById(R.id.connect_disconnect_button);

            showActivityGraphsButton.setEnabled(false);
            showActivityGraphs.setVisibility(View.GONE);


            /*
            fetchActivityDataBox = (LinearLayout) view.findViewById(R.id.device_action_fetch_activity_box);
            fetchActivityData = (ImageView) view.findViewById(R.id.device_action_fetch_activity);
            busyIndicator = (ProgressBar) view.findViewById(R.id.device_busy_indicator);
            takeScreenshotView = (ImageView) view.findViewById(R.id.device_action_take_screenshot);
            manageAppsView = (ImageView) view.findViewById(R.id.device_action_manage_apps);
            setAlarmsView = (ImageView) view.findViewById(R.id.device_action_set_alarms);
            showActivityGraphs = (ImageView) view.findViewById(R.id.device_action_show_activity_graphs);
            deviceInfoView = (ImageView) view.findViewById(R.id.device_info_image);

            deviceInfoBox = (RelativeLayout) view.findViewById(R.id.device_item_infos_box);

            //overflow
            deviceInfoList = (ListView) view.findViewById(R.id.device_item_infos);
            findDevice = (ImageView) view.findViewById(R.id.device_action_find);
            removeDevice = (ImageView) view.findViewById(R.id.device_action_remove);



            fetchActivityData.setVisibility(View.GONE);
            fetchActivityDataBox.setVisibility(View.GONE);
            setAlarmsView.setVisibility(View.GONE);
            takeScreenshotView.setVisibility(View.GONE);
            manageAppsView.setVisibility(View.GONE);
            deviceInfoView.setVisibility(View.GONE);*/
        }

    }

    public void justifyListViewHeightBasedOnChildren(ListView listView) {
        ArrayAdapter adapter = (ArrayAdapter) listView.getAdapter();

        if (adapter == null) {
            return;
        }
        ViewGroup vg = listView;
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, vg);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
        listView.setLayoutParams(par);
        listView.requestLayout();
    }

    private String getUniqueDeviceName(GBDevice device) {
        String deviceName = device.getName();
        if (!isUniqueDeviceName(device, deviceName)) {
            if (device.getModel() != null) {
                deviceName = deviceName + " " + device.getModel();
                if (!isUniqueDeviceName(device, deviceName)) {
                    deviceName = deviceName + " " + device.getShortAddress();
                }
            } else {
                deviceName = deviceName + " " + device.getShortAddress();
            }
        }
        return deviceName;
    }

    private boolean isUniqueDeviceName(GBDevice device, String deviceName) {
        for (int i = 0; i < deviceList.size(); i++) {
            GBDevice item = deviceList.get(i);
            if (item == device) {
                continue;
            }
            if (deviceName.equals(item.getName())) {
                return false;
            }
        }
        return true;
    }

    private void showTransientSnackbar(int resource) {
        Snackbar snackbar = Snackbar.make(parent, resource, Snackbar.LENGTH_SHORT);

        View snackbarView = snackbar.getView();

// change snackbar text color
        int snackbarTextId = android.support.design.R.id.snackbar_text;
        TextView textView = (TextView) snackbarView.findViewById(snackbarTextId);
        //textView.setTextColor();
        //snackbarView.setBackgroundColor(Color.MAGENTA);
        snackbar.show();
    }

}
