package com.pasid.mibandcardio;

import android.content.Intent;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;
import com.pasid.mibandcardio._a.AppIntroSampleSlider;
import com.pasid.mibandcardio.activities.ControlCenterv2;

public class MyIntro extends AppIntro {
    // Please DO NOT override onCreate. Use init
    @Override
    public void init(Bundle savedInstanceState) {

        //adding the three slides for introduction app you can ad as many you needed
        addSlide(AppIntroSampleSlider.newInstance(R.layout.app_intro1));
        addSlide(AppIntroSampleSlider.newInstance(R.layout.app_intro2));

        // Show and Hide Skip and Done buttons
        showStatusBar(false);
        showSkipButton(false);

        setDoneText(getString(R.string.done_button));

        // Turn vibration on and set intensity
        // You will need to add VIBRATE permission in Manifest file
        setVibrate(true);
        setVibrateIntensity(50);

        //Add animation to the intro slider
        setDepthAnimation();



        Thread t = new Thread(new Runnable() {

            @Override
            public void run() {



            }
        });
        t.start();
    }

    @Override
    public void onSkipPressed() {
        // Do something here when users click or tap on Skip button.
        //Toast.makeText(getApplicationContext(), "SKIPPING>>>>", Toast.LENGTH_SHORT).show();
        //Intent i = new Intent(getApplicationContext(), DiscoveryActivity.class);
        //startActivity(i);
    }

    @Override
    public void onNextPressed() {
        // Do something here when users click or tap on Next button.
    }

    @Override
    public void onDonePressed() {
        // Do something here when users click or tap tap on Done button.
        MyIntro.this.startActivity(new Intent(MyIntro.this, ControlCenterv2.class));
        MyIntro.this.finish();
    }

    @Override
    public void onSlideChanged() {
        // Do something here when slide is changed
    }

    @Override
    public void onBackPressed() {

    }
}
