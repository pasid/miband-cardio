/*  Copyright (C) 2015-2017 Andreas Shimokawa, Carsten Pfeiffer

    This file is part of Gadgetbridge.

    Gadgetbridge is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gadgetbridge is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */
package com.pasid.mibandcardio.activities.charts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import at.grabner.circleprogress.CircleProgressView;
import com.pasid.mibandcardio.GBApplication;
import com.pasid.mibandcardio.R;
import com.pasid.mibandcardio._a._utils.Ivan_Utils;
import com.pasid.mibandcardio.activities.DebugActivity;
import com.pasid.mibandcardio.database.DBHandler;
import com.pasid.mibandcardio.impl.GBDevice;
import com.pasid.mibandcardio.model.ActivitySample;
import com.pasid.mibandcardio.model.DeviceService;
import com.pasid.mibandcardio.model.Measurement;

public class LiveActivityFragment extends AbstractChartFragment {

    private BarLineChartBase mStepsPerMinuteHistoryChart;
    private ScheduledExecutorService pulseScheduler;
    private List<Measurement> heartRateValues;
    private LineDataSet mHistorySet;
    private LineDataSet mHeartRateSet;
    private LineDataSet mMaxValueSet;
    private LineDataSet mMinValueSet;
    private int mHeartRate;
    private TimestampTranslation tsTranslation;

    private ArrayList<Integer> colorList;

    private double maxOutlier;

    private TextView maxHeartRateTv, currentHeartrate, currentStatus;
    private CircleProgressView circleProgressView;
    private ImageView zoomInButton, zoomOutButton;

    private TextView currentStatusActual;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action){
                case DeviceService.ACTION_REALTIME_SAMPLES: {
                    ActivitySample sample = (ActivitySample) intent.getSerializableExtra(DeviceService.EXTRA_REALTIME_SAMPLE);
                    addSample(sample);
                    break;
                }
            }
        }
    };

    private void addSample(ActivitySample sample) {
        int heartRate = sample.getHeartRate();
        int timestamp = tsTranslation.shorten(sample.getTimestamp());
        //Toast.makeText(getActivity(), "Timestamp: " + timestamp, Toast.LENGTH_SHORT).show();

        //Toast.makeText(getView().getContext(), "HeartRate: " +heartRate, Toast.LENGTH_SHORT).show();

        if (isValidHeartRateValue(heartRate)) {
            setCurrentHeartRate(heartRate, timestamp);
        }
    }

    private int translateTimestamp(long tsMillis) {

        //long millis = System.currentTimeMillis();

        /*
        long days = TimeUnit.MILLISECONDS.toDays(millis);
        millis -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);
        */
        //long hours = TimeUnit.MILLISECONDS.toHours(tsMillis) % 24;
        //long minutes = TimeUnit.MILLISECONDS.toMinutes(tsMillis) % 60;
        //long seconds = TimeUnit.MILLISECONDS.toSeconds(tsMillis) % 60;

        //Toast.makeText(getContext(), " " + hours + " " + minutes, Toast.LENGTH_SHORT).show();


        int timestamp = (int) (tsMillis / 1000); // translate to seconds
        //Toast.makeText(getActivity(), "Timestamp: " + timestamp, Toast.LENGTH_SHORT).show();
        return tsTranslation.shorten(timestamp); // and shorten
        //return tsTranslation.shorten((int) (seconds));

        //int translation = (int) (millis / 1000);
        //return (translation); // and shorten
    }

    private void setCurrentHeartRate(int heartRate, int timestamp) {
        addHistoryDataSet(true);
        mHeartRate = heartRate;
    }

    private int getCurrentHeartRate() {
        int result = mHeartRate;
        mHeartRate = -1;
        return result;
    }

    private int globalIndex;


    private void addEntries(int timestamp) {

        double HearRateMaximal;

        if (!addHistoryDataSet(false)) {
            return;
        }

        if (mStepsPerMinuteHistoryChart.getData().getDataSets().size() > 1) {
            mStepsPerMinuteHistoryChart.setVisibility(View.VISIBLE);
            mStepsPerMinuteHistoryChart.setTouchEnabled(true);
        }

        int heartRate = getCurrentHeartRate();

        if (heartRate < 0) {
            //Toast.makeText(getView().getContext(), "HeartRate: " +heartRate, Toast.LENGTH_SHORT).show();
            heartRate = 0;
        }

        int normalColor = Color.parseColor("#636363"); // Preto
        int outlierColor = Color.parseColor("#bc3131"); // Vermelho

        if (heartRate > 0) {

            if (heartRate > maxOutlier)
                Ivan_Utils.generateHeartRateLog(getContext(), Integer.toString(heartRate));

            mHeartRate = heartRate;

            currentStatusActual.setText(String.valueOf(heartRate));

            if (heartRate >= maxOutlier) {
                colorList.add(outlierColor);
                //Aqui ação quando o pico aconteçe


                /*
                AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                alertDialog.setTitle("Alerta");
                alertDialog.setMessage("Seu batimento cardíaco excedeu o valor máximo!");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(android.R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                 */
                //Ivan_Utils.generateHeartRateLog(Integer.toString(heartRate));
            }
            else
                colorList.add(normalColor);

            //mHeartRateSet.addEntry(new Entry(timestamp, heartRate));
            mMinValueSet.addEntry(new Entry(globalIndex, (float) 40));
            mHeartRateSet.addEntry(new Entry(globalIndex, heartRate));
            mMaxValueSet.addEntry(new Entry(globalIndex, (float) maxOutlier));

            globalIndex++;
        }

    }

    private boolean addHistoryDataSet(boolean force) {
        if (mStepsPerMinuteHistoryChart.getData() == null) {
            // ignore the first default value to keep the "no-data-description" visible
            //if (force || mSteps.getTotalSteps() > 0) {

            if (force) {
                LineData data = new LineData();
                //data.addDataSet(mHistorySet);
                data.addDataSet(mMinValueSet);
                data.addDataSet(mHeartRateSet);
                data.addDataSet(mMaxValueSet);

                zoomInButton.setEnabled(true);
                zoomOutButton.setEnabled(true);

                mStepsPerMinuteHistoryChart.setData(data);
                return true;
            }


            return false;
        }
        return true;
    }


    private float circleSize = 6f;
    private float circleHoleSize = 4f;
    private int maxVisibleCount = 10;
    private float textSize = 18f;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        //LiveActivityFragment.this.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
        final IntentFilter filterLocal = new IntentFilter();
        filterLocal.addAction(DeviceService.ACTION_REALTIME_SAMPLES);

        globalIndex = 0;

        heartRateValues = new ArrayList<>();
        tsTranslation = new TimestampTranslation();
        colorList = new ArrayList<Integer>();

        String[] userInfo = Ivan_Utils.getUser(getContext());
        int idade = Integer.parseInt(userInfo[1]);
        maxOutlier = 208 - (0.7 * idade);
        //Toast.makeText(getContext(), "Max: " +maxOutlier, Toast.LENGTH_SHORT).show();

        View rootView = inflater.inflate(R.layout.fragment_live_activity, container, false);

        mStepsPerMinuteHistoryChart = (BarLineChartBase) rootView.findViewById(R.id.livechart_steps_per_minute_history);

        enableRealtimeTracking(true);
        setupHistoryChart2(mStepsPerMinuteHistoryChart);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mReceiver, filterLocal);
        mStepsPerMinuteHistoryChart.setVisibility(View.VISIBLE);

        /*
        enableRealtimeTracking(true);
        setupHistoryChart(mStepsPerMinuteHistoryChart);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mReceiver, filterLocal);
        mStepsPerMinuteHistoryChart.setVisibility(View.VISIBLE); */

        circleProgressView = (CircleProgressView) rootView.findViewById(R.id.circleView);
        maxHeartRateTv = (TextView) rootView.findViewById(R.id.realtime_activity_max_hr);
        currentHeartrate = (TextView) rootView.findViewById(R.id.realtime_activity_current_hearRate);
        currentStatus = (TextView) rootView.findViewById(R.id.realtime_activity_current_status);
        currentStatusActual = (TextView) rootView.findViewById(R.id.realtime_activity_current_status_actual);

        zoomInButton = (ImageView) rootView.findViewById(R.id.live_activity_zoom_in);
        zoomOutButton = (ImageView) rootView.findViewById(R.id.live_activity_zoom_out);
        zoomInButton.setEnabled(false);
        zoomOutButton.setEnabled(false);

        maxHeartRateTv.setText( (int) maxOutlier  + " " + getString(R.string.heart_rate_postfix));

        zoomInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Zoom In
                float xPos = mStepsPerMinuteHistoryChart.getX();
                float yPos = mStepsPerMinuteHistoryChart.getY();
                System.out.println("Valores Atuais: " + xPos +" e " + yPos);
                mStepsPerMinuteHistoryChart.zoomIn();
            }
        });

        zoomOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Zoom Out
                float xPos = mStepsPerMinuteHistoryChart.getX();
                float yPos = mStepsPerMinuteHistoryChart.getY();
                System.out.println("Valores Atuais: " + xPos +" e " + yPos);
                mStepsPerMinuteHistoryChart.zoomOut();

            }
        });

        // Configure circle view

        circleProgressView.setBarColor(Color.parseColor("#FF3FAC74"), Color.parseColor("#FF3FAC74"), Color.parseColor("#FFCCAD00"), Color.parseColor("#FFDA5400"), Color.parseColor("#FF871C1E"));
        circleProgressView.setMaxValue((float) (maxOutlier + (maxOutlier/2)) );
        circleProgressView.setUnitVisible(false);

        circleProgressView.setClickable(false);
        circleProgressView.setSeekModeEnabled(false);
        circleProgressView.setOnProgressChangedListener(new CircleProgressView.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(float value) {

                //mStepsPerMinuteHistoryChart.moveViewToX(mStepsPerMinuteHistoryChart.getX() + 1);

                String currentHR = " " + mHeartRate + " " + getString(R.string.heart_rate_postfix);
                String statusNow = getString(R.string.ivan_verifying);
                currentHeartrate.setText(currentHR);

                //if (mHeartRate <= maxOutlier)
                if (value <= maxOutlier) {
                    statusNow = getString(R.string.ivan_hr_normal);
                    currentStatus.setTextColor(Color.parseColor("#321321"));
                }
                else {
                    statusNow = getString(R.string.ivan_hr_above_normal);
                    DebugActivity.sendNotificationMiBand(0);
                    currentStatus.setTextColor(Color.parseColor("#bc3131"));
                }

                currentStatus.setText(statusNow);
            }
        });

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        mStepsPerMinuteHistoryChart.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, (height/2) ));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, (height/6));
        params.gravity = Gravity.CENTER_HORIZONTAL;
        circleProgressView.setLayoutParams(params);

        circleSize = (float) width / 10;
        circleHoleSize = circleSize - circleSize/20;
        textSize = (float) width /30;

        double maxOutlier = this.maxOutlier + 40;
        ((TextView) rootView.findViewById(R.id.textViewMaxOutlier)).setText("" + (int) maxOutlier);
        ((TextView) rootView.findViewById(R.id.textViewMaxOutlier50)).setText("" + (int) ((maxOutlier/4) + 20));
        ((TextView) rootView.findViewById(R.id.textViewMaxOutlier100)).setText("" + (int) ((maxOutlier/2) + 20));
        ((TextView) rootView.findViewById(R.id.textViewMaxOutlier150)).setText("" +   (int) ((maxOutlier - (maxOutlier/4)) + 20));

        //getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        //textSize = (float) width / 2;
        return rootView;
    }

    @Override
    public void onPause() {
        enableRealtimeTracking(false);
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        enableRealtimeTracking(true);
    }

    private ScheduledExecutorService startActivityPulse(){

        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();

        service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {

                FragmentActivity activity = LiveActivityFragment.this.getActivity();

                if (activity != null && !activity.isFinishing() && !activity.isDestroyed()){
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pulse();
                        }
                    });
                }

            }
        }, 0, getPulseIntervalMillis(), TimeUnit.MILLISECONDS);

        return service;
    }

    private void stopActivityPulse() {
        if (pulseScheduler != null) {
            pulseScheduler.shutdownNow();
            pulseScheduler = null;
        }
    }

    /**
     * Called in the UI thread.
     */
    float sX = 0f, sY = 0f, x = 0f, y = 0f;

    private void pulse() {
        addEntries(translateTimestamp(System.currentTimeMillis()));

        LineData historyData = (LineData) mStepsPerMinuteHistoryChart.getData();

        if (historyData == null) {
            return;
        }

        historyData.notifyDataChanged();
        mStepsPerMinuteHistoryChart.notifyDataSetChanged();

        if (mHeartRate > 30) {
            circleProgressView.setValue(mHeartRate);
        }
        renderCharts();

        // have to enable it again and again to keep it measureing
        GBApplication.deviceService().onEnableRealtimeHeartRateMeasurement(true);

    }

    private int getPulseIntervalMillis() {

        // Aqui se define o tempo de cada medição automática
        //return 1000;
        //return 40;
        return 1500;

    }

    @Override
    protected void onMadeVisibleInActivity() {
        super.onMadeVisibleInActivity();

        //enableRealtimeTracking(true);
    }

    private void enableRealtimeTracking(boolean enable) {
        if (enable && pulseScheduler != null) {
            // already running
            return;
        }

        GBApplication.deviceService().onEnableRealtimeSteps(enable);
        GBApplication.deviceService().onEnableRealtimeHeartRateMeasurement(enable);
        if (enable) {
            if (getActivity() != null) {
                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
            pulseScheduler = startActivityPulse();
        } else {
            stopActivityPulse();
            if (getActivity() != null) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        }
    }

    @Override
    protected void onMadeInvisibleInActivity() {
        enableRealtimeTracking(false);
        super.onMadeInvisibleInActivity();
    }

    @Override
    public void onDestroyView() {
        onMadeInvisibleInActivity();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mReceiver);
        super.onDestroyView();
    }

    private BarDataSet setupCurrentChart(CustomBarChart chart, BarEntry entry, String title) {
        //mStepsPerMinuteCurrentChart.getAxisLeft().setAxisMaxValue(MAX_STEPS_PER_MINUTE);
        return setupCommonChart(chart, entry, title);
    }

    private BarDataSet setupCommonChart(CustomBarChart chart, BarEntry entry, String title) {
        /*

        String[] userInfo = Ivan_Utils.getUser();

        int idade = Integer.parseInt(userInfo[1]);
        String sexo = userInfo[2];

        if (idade >= 10 && idade<=29){

            if(sexo.equalsIgnoreCase("Masculino\n")){
                maxOutlier = 86;
                minOutlier = 66;
            }
            else{
                maxOutlier = 91;
                minOutlier = 75;
            }

        }

        else if(idade<=49){

            if(sexo.equalsIgnoreCase("Masculino\n")){
                maxOutlier = 83;
                minOutlier = 69;
            }
            else{
                maxOutlier = 86;
                minOutlier = 72;
            }

        }

        else if(idade<=69){

            if(sexo.equalsIgnoreCase("Masculino\n")){
                maxOutlier = 89;
                minOutlier = 67;
            }
            else{
                maxOutlier = 84;
                minOutlier = 64;
            }

        }
        else if(idade>18){

            if(sexo.equalsIgnoreCase("Masculino\n")){
                maxOutlier = 83;
                minOutlier = 61;
            }
            else{
                maxOutlier = 81;
                minOutlier = 75;
            }

        }
*/
        chart.setSinglAnimationEntry(entry);

//        chart.getXAxis().setPosition(XAxis.XAxisPosition.TOP);
        chart.getXAxis().setDrawLabels(false);
        chart.getXAxis().setEnabled(false);
        chart.getXAxis().setTextColor(CHART_TEXT_COLOR);
        chart.getAxisLeft().setTextColor(CHART_TEXT_COLOR);

        chart.setBackgroundColor(BACKGROUND_COLOR);
        chart.getDescription().setTextColor(DESCRIPTION_COLOR);
        chart.getDescription().setText(title);
//        chart.setNoDataTextDescription("");
        chart.setNoDataText("");
        chart.getAxisRight().setEnabled(false);

        List<BarEntry> entries = new ArrayList<>();
        List<Integer> colors = new ArrayList<>();

        entries.add(new BarEntry(0, 0));
        entries.add(entry);
        entries.add(new BarEntry(2, 0));
        colors.add(akActivity.color);
        colors.add(akActivity.color);
        colors.add(akActivity.color);
//        //we don't want labels
//        xLabels.add("");
//        xLabels.add("");
//        xLabels.add("");

        BarDataSet set = new BarDataSet(entries, "");
        set.setDrawValues(false);
        set.setColors(colors);
        BarData data = new BarData(set);
//        data.setGroupSpace(0);
        chart.setData(data);

        chart.getLegend().setEnabled(false);
        chart.setVisibility(View.GONE);

        return set;
    }

    private BarDataSet setupTotalStepsChart(CustomBarChart chart, BarEntry entry, String label) {
        //mTotalStepsChart.getAxisLeft().setAxisMaximum(5000); // TODO: use daily goal - already reached steps
        return setupCommonChart(chart, entry, label); // at the moment, these look the same
    }

    private void setupHistoryChart2(BarLineChartBase chart) {

        configureBarLineChartDefaults(chart);
        // Toast.makeText(getView().getContext(), "Can Write: " +CanWriteInChart, Toast.LENGTH_SHORT).show();
        chart.setPinchZoom(false);
        chart.setTouchEnabled(false); // no zooming or anything if there is no data yet
        chart.setDoubleTapToZoomEnabled(false); // won't zoom when double tapped,
        chart.setBackgroundColor(BACKGROUND_COLOR);
        chart.getDescription().setTextColor(DESCRIPTION_COLOR);

        //String show = "Max HR: " + (int) HearRateMaximal + " BPM.";
        //String show = "HRV: "+ minOutlier + " - " + maxOutlier + "    HRMax: "+ (int)HearRateMaximal;

        //chart.getDescription().setText(show);
        //chart.getDescription().setTextSize(16);
        //chart.setNoDataText(show);

        chart.getLegend().setEnabled(false);
        Paint infoPaint = chart.getPaint(Chart.PAINT_INFO);
        infoPaint.setTextSize(Utils.convertDpToPixel(20f));
        infoPaint.setFakeBoldText(true);
        chart.setPaint(infoPaint, Chart.PAINT_INFO);
        chart.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

        XAxis x = chart.getXAxis();
        x.setDrawLabels(false);
        x.setDrawGridLines(false);
        x.setEnabled(true);
        x.setTextColor(CHART_TEXT_COLOR);
        x.setDrawLimitLinesBehindData(false);

        YAxis yAxisLeft = chart.getAxisLeft();
        yAxisLeft.setEnabled(true);
        yAxisLeft.setDrawLabels(false);
        yAxisLeft.setDrawGridLines(false);
        yAxisLeft.setDrawTopYLabelEntry(false);
        yAxisLeft.setTextColor(CHART_TEXT_COLOR);
        yAxisLeft.setAxisMinimum(0);

        YAxis yAxisRight = chart.getAxisRight();
        yAxisRight.setEnabled(true);
        yAxisRight.setDrawLabels(true);
        yAxisRight.setDrawGridLines(false);
        yAxisRight.setDrawTopYLabelEntry(true);
        yAxisRight.setTextColor(CHART_TEXT_COLOR);

        yAxisRight.setAxisMaximum((float) maxOutlier + 40);
        yAxisRight.setAxisMinimum((float) 40);

        //chart.setVisibleXRangeMinimum(1);
        chart.setScaleMinima(1f, 1f);

        int fillColor = Color.parseColor("#FFFFFF");
        int lineColor = Color.parseColor("#AFA7A3");
        int bottomLineColor = Color.parseColor("#AFA7A3");

        mMinValueSet = createHeartrateSet(new ArrayList<Entry>(), "");
        mMinValueSet.setAxisDependency(YAxis.AxisDependency.RIGHT);
        mMinValueSet.setLineWidth(2.6f);
        mMinValueSet.setDrawValues(false);
        mMinValueSet.setDrawCircles(false);
        mMinValueSet.setDrawCircleHole(false);
        mMinValueSet.setColor(bottomLineColor);
        mMinValueSet.setDrawFilled(false);

        mMaxValueSet = createHeartrateSet(new ArrayList<Entry>(), "");
        mMaxValueSet.setAxisDependency(YAxis.AxisDependency.RIGHT);
        mMaxValueSet.setLineWidth(2.6f);
        mMaxValueSet.setDrawValues(false);
        mMaxValueSet.setDrawCircles(false);
        mMaxValueSet.setDrawCircleHole(false);
        mMaxValueSet.setDrawFilled(false);

        mHeartRateSet = createHeartrateSet(new ArrayList<Entry>(), getString(R.string.live_activity_heart_rate));
        mHeartRateSet.setLineWidth(2.6f);
        mHeartRateSet.setDrawValues(true);
        mHeartRateSet.setValueTextSize(textSize);
        mHeartRateSet.setDrawCircles(true);
        mHeartRateSet.setCircleSize(circleSize);
        mHeartRateSet.setCircleHoleRadius(circleHoleSize);
        mHeartRateSet.setDrawCircleHole(true);
        mHeartRateSet.setDrawFilled(true);

        mHeartRateSet.setColor(lineColor);
        mHeartRateSet.setFillColor(fillColor);
        mHeartRateSet.setCircleColors(colorList);

        chart.setMaxVisibleValueCount(100);
    }

    @Override
    public String getTitle() {
        return getContext().getString(R.string.liveactivity_live_activity);
    }

    @Override
    protected void showDateBar(boolean show) {
        // never show the data bar
        super.showDateBar(false);
    }

    @Override
    protected void refresh(){
        // do nothing, we don't have any db interaction
    }

    @Override
    protected ChartsData refreshInBackground(ChartsHost chartsHost, DBHandler db, GBDevice device) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void updateChartsnUIThread(ChartsData chartsData) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void renderCharts() {
        //mStepsPerMinuteCurrentChart.animateY(150);
        //mTotalStepsChart.animateY(150);
        mStepsPerMinuteHistoryChart.invalidate();
        //mStepsPerMinuteHistoryChart.zoom(mStepsPerMinuteHistoryChart.getScaleX() + 0.5f, 0f, mStepsPerMinuteHistoryChart.getVisibleXRange(), 0f);
    }

    @Override
    protected List<ActivitySample> getSamples(DBHandler db, GBDevice device, int tsFrom, int tsTo) {
        throw new UnsupportedOperationException("no db access supported for live activity");
    }

    @Override
    protected void setupLegend(Chart chart) {
        // no legend
    }
}
