package com.pasid.mibandcardio._a;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.pasid.mibandcardio.R;
import com.pasid.mibandcardio._a._utils.Ivan_Utils;
import com.pasid.mibandcardio.activities.ControlCenterv2;

public class FormularioIvan extends AppCompatActivity implements View.OnClickListener {

    private Spinner spinner;
    private EditText name, age;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_ivan);

        spinner = (Spinner) findViewById(R.id.sp_gender);
        button = (Button) findViewById(R.id.b_save_dt);
        name = (EditText) findViewById(R.id.et_name);
        age = (EditText) findViewById(R.id.et_age);

        button.setOnClickListener(this);

        String[] genders = new String[2];
        genders[0] = getString(R.string.male_gender);
        genders[1] = getString(R.string.female_gender);

        ArrayAdapter<String> spinnerArray = new ArrayAdapter<>(FormularioIvan.this, android.R.layout.simple_spinner_item, genders);
        spinnerArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArray);

        //
        String userData[] = Ivan_Utils.getUser(this);
        if (userData.length > 2) {
            name.setText(userData[0]);
            age.setText(userData[1]);

            if (userData[2].equalsIgnoreCase(getString(R.string.male_gender) + "\n") || userData[2].equalsIgnoreCase(getString(R.string.male_gender)))
                spinner.setSelection(0);
            else
                spinner.setSelection(1);
        }

        // Set up the input

        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        //input.setInputType(InputType.TYPE_CLASS_TEXT);
        int maxLength = 41;
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(maxLength);
        name.setFilters(FilterArray);

    }

    @Override
    public void onClick(View view) {
        String name, age, gender;
        name = this.name.getText().toString();
        age  = this.age.getText().toString();
        gender = this.spinner.getSelectedItem().toString();

        if (age.isEmpty())
            this.age.setError(getString(R.string.ivan_error_age));
        else if (Integer.parseInt(age) < 18 || Integer.parseInt(age) > 100)
            this.age.setError(getString(R.string.ivan_error_age));
        else {
            if (name.isEmpty())
                name = getString(R.string.user_info_not_informed);

            Ivan_Utils.saveUserInformation(this, name, age, gender);
            startActivity(new Intent(this, ControlCenterv2.class));
            finish();
        }

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, ControlCenterv2.class));
        finish();
    }
}
