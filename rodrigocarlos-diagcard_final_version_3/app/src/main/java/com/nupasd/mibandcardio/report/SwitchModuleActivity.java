package com.nupasd.mibandcardio.report;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.nupasd.mibandcardio.R;
import com.nupasd.mibandcardio.activities.ControlCenterv2;

public class SwitchModuleActivity extends AppCompatActivity
implements View.OnClickListener{

    private Button btnMonitoramento, btnRecepcao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch_module);

        btnMonitoramento = (Button) findViewById(R.id.btn_monitoramento);
        btnRecepcao = (Button) findViewById(R.id.btn_recepcao);

        btnMonitoramento.setOnClickListener(this);
        btnRecepcao.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_monitoramento:
                startActivity(new Intent(this, ControlCenterv2.class));
                break;
            case R.id.btn_recepcao:
                startActivity(new Intent(this, RecepcaoActivity.class));
                break;
        }
    }
}
