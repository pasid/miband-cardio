package com.nupasd.mibandcardio.report;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by HILTON on 13/03/2018.
 */

public class Util {
    public static boolean serviceIsRunning(Context ctx, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void saveToFirebase(String[] data){
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("/");
        mDatabase.push().setValue(data);

    }

    public static String getCodeIntegration(){
        return "-L7XHIIipRoeoqc-jBrP";
    }
}
