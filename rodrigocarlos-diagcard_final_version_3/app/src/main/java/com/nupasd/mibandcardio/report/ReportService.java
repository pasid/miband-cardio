package com.nupasd.mibandcardio.report;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nupasd.mibandcardio.R;

import java.util.ArrayList;

public class ReportService extends Service {
    private Thread reportServiceThread;

    public ReportService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        reportServiceThread = new Thread(new ExportLocalData());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        reportServiceThread.start();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {

        reportServiceThread.stop();
        reportServiceThread = null;

        super.onDestroy();
    }

    private final class ExportLocalData implements Runnable{

        // 1h = 3600000L

        private static final long TEST_TIME =  60000L;
        private static final long TIME =  3600000L;
        private final Handler handler;

        private DatabaseReference mDatabase;

        public ExportLocalData() {

            this.handler = new Handler(Looper.getMainLooper());
            mDatabase = FirebaseDatabase.getInstance().getReference("/");

        }

        @Override
        public void run() {
            while (true){
                SystemClock.sleep(TEST_TIME);
                //Envia os dados coletados na ultima hora
                sendData();
            }
        }

        private void sendData() {
            ArrayList<Data> data = FileHelper.ReadFile(getApplicationContext());
            if(!data.isEmpty()){
                sendToCloud( getApplicationContext(), data);

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        //Atualiza a UI sobre o processo finalizado
                        notifyUser();
                    }
                });
            }

        }

        private void sendToCloud(Context ctx, ArrayList<Data> data){
            SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(ctx);

            String codigoIntegracao = Util.getCodeIntegration();
            for(Data d: data) {
                mDatabase.child(codigoIntegracao).child("data").push().setValue(d);
            }
        }

        private void notifyUser(){
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(getApplicationContext())
                            .setSmallIcon(R.drawable.ic_notification)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText("Os dados da última hora foram exportados!");

            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(001, mBuilder.build());
        }
    }


}
