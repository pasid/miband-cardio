package com.nupasd.mibandcardio.report;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.nupasd.mibandcardio.R;

import java.util.ArrayList;

public class RecepcaoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recepcao);

        ListView lista = (ListView) findViewById(R.id.lista);

        final ArrayList<Data> data = new ArrayList<>();

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("/");
        mDatabase.child(Util.getCodeIntegration()).child("data").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                data.add(new Gson().fromJson(dataSnapshot.getValue().toString(), Data.class));

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        ArrayAdapter<Data> adapter = new ArrayAdapter<Data>(this,
                android.R.layout.simple_list_item_1, data);
        lista.setAdapter(adapter);
        lista.deferNotifyDataSetChanged();

    }
}
