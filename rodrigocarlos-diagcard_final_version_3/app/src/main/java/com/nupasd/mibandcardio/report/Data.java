package com.nupasd.mibandcardio.report;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Created by HILTON on 12/03/2018.
 */

public class Data implements Serializable{

    private int batimentos;
    private String timestamp;

    public Data(String timestamp, int batimentos) {
        this.timestamp = timestamp;
        this.batimentos = batimentos;
    }

    public Data(int batimentos) {
        this.timestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        this.batimentos = batimentos;
    }

    public int getBatimentos() {
        return this.batimentos;
    }

    public String getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        String date = new SimpleDateFormat("dd/MM/yyyy HH:MM").format(Long.valueOf(timestamp));

        return "Data: " +date.toString()+ ", Batimentos: "+String.valueOf(batimentos) +" bpm";
    }
}
