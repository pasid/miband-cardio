package com.nupasd.mibandcardio.mibandserver;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.nupasd.mibandcardio._a._utils.Ivan_Utils;

import java.sql.Timestamp;

import io.realm.Realm;

/**
 * Created by HILTON on 30/04/2018.
 */

public class KUtils {
    private static final String STORE_LAST_AVERAGE = "last_average";
    public static void setAverage(Context ctx, int average){
        SharedPreferences spf = init(ctx);
        SharedPreferences.Editor ed = spf.edit();

        int count = spf.getInt("count", 0);
        count++;

        int sum = spf.getInt("sum", 0);
        sum += average;

        int avm = sum/count;

        ed.putInt("average", avm);
        ed.putInt("count", count);
        ed.putInt("sum", sum);
        ed.commit();

        Log.i("TOP", "Contador: "+count);
        Log.i("TOP", "Soma: "+sum);
        Log.i("TOP", "Média: "+avm);
    }

    public static int getAverage(Context ctx){
        SharedPreferences spf = init(ctx);
        return spf.getInt("average", 0);
    }

    private static SharedPreferences init(Context ctx) {
        return ctx.getSharedPreferences(STORE_LAST_AVERAGE, Context.MODE_PRIVATE);
    }

    public static void clear(Context ctx){
        SharedPreferences spf = init(ctx);
        SharedPreferences.Editor ed = spf.edit();
        ed.clear();
        ed.commit();
    }

    public static void saveDataInRelamDB(Context ctx){
        int average = KUtils.getAverage(ctx);
        long tms = new Timestamp(System.currentTimeMillis()).getTime();
        String user[] = Ivan_Utils.getUser(ctx);
        String userName = user[0];
        int userAge = Integer.parseInt(user[1]);
        String userSex = user[2];

        StdModel stdModel = new StdModel();

        UserModel userModel = new UserModel();
        userModel.name = userName;
        userModel.age = userAge;
        userModel.sex = userSex;

        DataModel dataModel = new DataModel();
        dataModel.timestamp = tms;
        dataModel.average = average;

        stdModel.user = userModel;
        stdModel.data = dataModel;

        Realm realm = Realm.getDefaultInstance();
        try {
            realm.beginTransaction();
            realm.copyToRealm(stdModel);
            realm.commitTransaction();
            Log.i("TOP", "Dados inseridos no banco!");
        }finally {
            realm.close();
        }
    }
}
