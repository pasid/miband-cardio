package com.nupasd.mibandcardio.mibandserver;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.CountDownTimer;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by HILTON on 30/04/2018.
 */

public class Timer extends CountDownTimer {
    private static Context ctx;
    public static final long ONE_SECOND = 1000L;
    public static final long ONE_MINUTE = 60000L;
    public static final long ONE_HOUR = (60000L * 60);
    public static final long TWENTY_FOUR_HOUR = (ONE_HOUR * 24);

    private static SharedPreferences spf;
    private boolean isSec = false;

    public Timer(Context ctx, long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
        this.ctx = ctx;
        spf = ctx.getSharedPreferences("server_info", Context.MODE_PRIVATE);
        if (spf.getInt("interval", 2) == 0) {
            isSec = true;
        }
    }

    @Override
    public void onTick(long millisUntilFinished) {
        //A cada uma hora ou um minuto ou um segundo enviar para o servidor.
        if (isConnected(ctx)) {
            if (isSec) {
                //Envia a media atual a cada segundo
                int average = KUtils.getAverage(ctx);
                if (average > 0) {
                    Log.i("TOP", "Enviou! - " + average);
                    KUtils.clear(ctx);
                }
            } else {
                Realm realm = Realm.getDefaultInstance();
                try {
                    //Pega do banco Realm e envia
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<StdModel> results = realm.where(StdModel.class).findAll();
                            if(results.size() > 0){
                                WebClient c = new WebClient(ctx, getJSON(results));
                                c.execute();
                                results.deleteAllFromRealm();
                                Log.i("TOP", "Enviou!");
                            }
                        }
                    });

                } finally {
                    realm.close();
                }
            }
        }
    }

    private String getJSON(RealmResults<StdModel> results) {
        JSONArray root = new JSONArray();
        try {
        for (StdModel m: results) {
            JSONObject obj = new JSONObject();
            obj.put("average", m.data.average);
            obj.put("timestamp", m.data.timestamp);
            obj.put("name", m.user.name);
            obj.put("age", m.user.age);
            obj.put("sex", m.user.sex);
            root.put(obj);
        }
        } catch (JSONException e) {
            return "";
        }
        return  root.toString();
    }

    private void deleteAll() {
        Realm realm = Realm.getDefaultInstance();
        try {
            realm.beginTransaction();
            realm.delete(StdModel.class);
            realm.commitTransaction();
        } finally {
            realm.close();
        }
    }

    @Override
    public void onFinish() {
        //A cada 24 horas todos os dados do banco Realm são apagados( para não enviezar as 24h seguintes).
        deleteAll();
        this.cancel();
        System.gc();
        if (spf.getInt("interval", 2) == 0) {
            new Timer(ctx, TWENTY_FOUR_HOUR, ONE_SECOND);
        } else if (spf.getInt("interval", 2) == 1) {
            new Timer(ctx, TWENTY_FOUR_HOUR, ONE_MINUTE);
        } else {
            new Timer(ctx, TWENTY_FOUR_HOUR, ONE_HOUR);
        }
    }

    private static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectivityManager != null) {
            networkInfo =
                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        }
        return networkInfo == null ? false : networkInfo.isConnected();
    }
}
