package com.nupasd.mibandcardio.mibandserver;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;

public class TimerSyncService extends Service {
    private Timer timer;
    private SharedPreferences spf;

    public TimerSyncService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        spf = this.getSharedPreferences("server_info", Context.MODE_PRIVATE);
        if(spf.getInt("interval", 2) == 0){
            timer = new Timer(this, Timer.TWENTY_FOUR_HOUR, Timer.ONE_SECOND);
        }else if(spf.getInt("interval", 2) == 1){
            timer = new Timer(this, Timer.TWENTY_FOUR_HOUR, Timer.ONE_MINUTE);
        }else{
            timer = new Timer(this, Timer.TWENTY_FOUR_HOUR, Timer.ONE_HOUR);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        timer.start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(timer != null){
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
