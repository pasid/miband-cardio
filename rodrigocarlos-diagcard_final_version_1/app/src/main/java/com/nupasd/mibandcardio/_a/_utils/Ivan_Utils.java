package com.nupasd.mibandcardio._a._utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by ivan on 01/06/17.
 */

public class Ivan_Utils {

    private static final String FILE_PATH = "heart_rate_outliers.txt";
    private static final String USER_PATH = "active_user_info.txt";
    private static final String STEPS_PATH = "steps_already.txt";
    //private static final String FILE_PATH = "/storage/emulated/0/heart_rate_outliers.txt";
    //private static final String USER_PATH = "/storage/emulated/0/active_user_info.txt";

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = { Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE };

    /*
    private static void writeToFile(String path, String data, boolean append) {
        try {
            FileWriter fw = new FileWriter(path, append);
            PrintWriter pw = new PrintWriter(fw, true);
            pw.printf(data + "\n");
        } catch (IOException e) { e.printStackTrace(); }
    }
    private static String readFromFile(String path) {
        StringBuilder builder = new StringBuilder();

        try{
            BufferedReader br = new BufferedReader(new FileReader(path));
            String line;
            while((line = br.readLine()) != null)
                builder.append(line + "\n");
            br.close();
        }catch(IOException ignored){}
        return builder.toString();
    }
    */

    private static String getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(cal.getTime());
    }

    //public methods
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
        }
    }

    public static void generateHeartRateLog(Context context, String heartRate) {
        writeToFile(context, FILE_PATH, getCurrentTime() + "&>" + heartRate, true);
    }

    public static float[] getOutliers(Context context) {
        String str = readFromFile(context, FILE_PATH);
        String[] strSplit = str.split("\n");
        float[] strReturn = new float[strSplit.length];
        for (int i = 0; i < strSplit.length; i++) {
            strReturn[i] = Float.parseFloat( strSplit[i].split("&>")[1] );
        }

        return strReturn;
    }

    public static void saveUserInformation(Context context, String name, String age, String gender) {
        writeToFile(context, USER_PATH, name + "&>" + age + "&>" + gender, false);
        //System.out.println(readFromFile(USER_PATH));
    }

    public static String[] getUser(Context context) {
        String[] user;
        user = readFromFile(context, USER_PATH).split("&>");
        return user;
    }

    public static void setStepsState(Context context, String state) {
        writeToFile(context, STEPS_PATH, state, false);
    }

    public static boolean getStepsState(Context context) {
        String steps = readFromFile(context, STEPS_PATH);
        if (!steps.isEmpty()) {
            if (steps.equalsIgnoreCase("true")) {
                return true;
            } else if (steps.equalsIgnoreCase("false")) {
                return false;
            }
        }
        return false;
    }


    //=====
    private static String readFromFile(Context context, String path) {
        String string = "";

        try {
            InputStream inputStream = context.openFileInput(path);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                string = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException ignored) { /*writeToFile(path, ""); string = readFromFile(path);*/ }
        catch (IOException ignored) { }

        return string;
    }

    private static void writeToFile(Context context, String path, String data, boolean append) {
        int mode = Context.MODE_PRIVATE;
        if (append) {
            mode = Context.MODE_APPEND;
        }
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(path, mode));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException ignored) {}
    }

}
