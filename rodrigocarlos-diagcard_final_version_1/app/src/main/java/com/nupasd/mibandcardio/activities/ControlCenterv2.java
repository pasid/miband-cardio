/*  Copyright (C) 2016-2017 Andreas Shimokawa, Carsten Pfeiffer, Daniele
    Gobbetti

    This file is part of Gadgetbridge.

    Gadgetbridge is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gadgetbridge is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */
package com.nupasd.mibandcardio.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.cketti.library.changelog.ChangeLog;

import com.nupasd.mibandcardio.GBApplication;
import com.nupasd.mibandcardio.R;
import com.nupasd.mibandcardio._a._utils.Ivan_Utils;
import com.nupasd.mibandcardio.adapter.GBDeviceAdapterv2;
import com.nupasd.mibandcardio.devices.DeviceManager;
import com.nupasd.mibandcardio.impl.GBDevice;
import com.nupasd.mibandcardio.mibandserver.SettingEnvironmentActivity;
import com.nupasd.mibandcardio.mibandserver.TimerSyncService;
import com.nupasd.mibandcardio.util.GB;
import com.nupasd.mibandcardio.util.Prefs;

//TODO: extend GBActivity, but it requires actionbar that is not available
public class ControlCenterv2 extends AppCompatActivity {
       // implements NavigationView.OnNavigationItemSelectedListener {

    //needed for KK compatibility
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private DeviceManager deviceManager;

    private List<GBDevice> deviceList;
    private GBDevice device;
    private ViewGroup homeParent;
    private View homeLayout;
    private GBDeviceAdapterv2 mGBDeviceAdapter;
    private RecyclerView deviceListView;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case GBApplication.ACTION_QUIT:
                    finish();
                    break;
                case DeviceManager.ACTION_DEVICES_CHANGED:
                    refreshPairedDevices();
                    break;
            }
        }
    };

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    private RelativeLayout bigDadLayout;
    private Button finishButton;
    private ImageView linearImage1, linearImage2;

    private boolean stepsState;
    private BluetoothAdapter adapter;
    private int greenColor, blueColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controlcenterv2);

        stepsState = Ivan_Utils.getStepsState(ControlCenterv2.this);

        // Initialize Views
        greenColor = Color.parseColor("#6acc61");
        blueColor = Color.parseColor("#61adcc");

        bigDadLayout = (RelativeLayout) findViewById(R.id.bigDadLayoutControlCenter);
        LinearLayout linearButton1 = (LinearLayout) findViewById(R.id.linearButtonOption1);
        LinearLayout linearButton2 = (LinearLayout) findViewById(R.id.linearButtonOption2);
        LinearLayout linearButton3 = (LinearLayout) findViewById(R.id.linearButtonOption3);
        finishButton = (Button) findViewById(R.id.controlCenterFinishButton);

        // Coisas importantes da API que devem ser feitas antes das Verificações Mestras
        //end of material design boilerplate
        deviceManager = ((GBApplication) getApplication()).getDeviceManager();

        deviceListView = (RecyclerView) findViewById(R.id.deviceListView);
        deviceListView.setHasFixedSize(true);
        deviceListView.setLayoutManager(new LinearLayoutManager(this));

        // BRACELET DEVICE
        deviceList = deviceManager.getDevices();
        mGBDeviceAdapter = new GBDeviceAdapterv2(this, this, deviceList);

        deviceListView.setAdapter(this.mGBDeviceAdapter);


        IntentFilter filterLocal = new IntentFilter();
        filterLocal.addAction(GBApplication.ACTION_QUIT);
        filterLocal.addAction(DeviceManager.ACTION_DEVICES_CHANGED);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, filterLocal);


        // Se algo der errado, colocar esse bloco abaixo do bloco a seguir
        if (GB.isBluetoothEnabled() && deviceList.isEmpty() && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            // Não apagar (chamar a tela de Discovery em celulares com android mais antigo)
            //startActivity(new Intent(this, DiscoveryActivity.class));
        } else {
            GBApplication.deviceService().requestDeviceInfo();
        }

        refreshPairedDevices();



        /*
         * Ask for permission to intercept notifications on first run.
         */
        Prefs prefs = GBApplication.getPrefs();
        //if (prefs.getBoolean("firstrun", true)) {
        /*if (prefs.getBoolean("firstrun", true)) {
            prefs.getPreferences().edit().putBoolean("firstrun", false).apply();
            Intent enableIntent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
            startActivity(enableIntent);
        }*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();
        }

        ChangeLog cl = new ChangeLog(this);
        if (cl.isFirstRun()) {
            //cl.getLogDialog().show();
            //Não mostrar o LOG
        }

        GBApplication.deviceService().start();

        //
        //
        // Verificações toda vez que o programa iniciar
        //
        //
        // Verifica se precisa mostrar a Tela de Passos
        //verifyBigDadLayout();

        /*
        verifyButton1();
        verifyButton2();
        verifyButton3();
        verifyFinishButton();*/

        //
        // Outras funções

        linearButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                asksForUserInfo();
            }
        });

        linearButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkBluetoothAvailable();
            }
        });

        linearButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchDiscoveryActivity();
            }
        });

        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Ivan_Utils.setStepsState(ControlCenterv2.this, "true"); // Agora não vai mais aparecer a Tela de Passos

                try {
                    getSupportActionBar().show();
                } catch(Exception ignored) {}

                deviceListView.setVisibility(View.VISIBLE);
                /*bigDadLayout.animate()
                        .translationY(bigDadLayout.getHeight())
                        .alpha(0.0f)
                        .setDuration(500);*/

                bigDadLayout.setVisibility(View.GONE);
                /*
                bigDadLayout.animate()
                        .translationY(bigDadLayout.getHeight())
                        .setDuration(400)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                bigDadLayout.setVisibility(View.GONE);
                                bigDadLayout.clearAnimation();
                            }
                        });*/
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void verifyButton1() {

        if ( Ivan_Utils.getUser(this).length > 2) {
            checkButton(R.id.linearImageOption1);
        } else {
            uncheckButton(R.id.linearImageOption1);
        }

        counter();
    }

    private void verifyButton2() {

        if (GB.isBluetoothEnabled()) {
            checkButton(R.id.linearImageOption2);
        } else {
            uncheckButton(R.id.linearImageOption2);
        }

        counter();
    }

    private void verifyButton3() {

        if ( ! deviceList.isEmpty() ) {
            checkButton(R.id.linearImageOption3);
        } else {
            uncheckButton(R.id.linearImageOption3);
        }

        counter();
    }

    private void verifyFinishButton() {

        if ( Ivan_Utils.getUser(this).length > 2 && GB.isBluetoothEnabled() && ! deviceList.isEmpty() ) {
            enableFinishButton();
        } else {
            disableFinishButton();
        }
    }

    private void verifyBigDadLayout() {

        //if (! stepsState) { // Tela de Passos ainda não foi mostrada



        //}
        /*else { // Tela de Passos já apareceu antes

            // Mostrar a Barra de Ação
            try {
                getSupportActionBar().show();
            } catch (Exception ignored) {
            }

            // Mostrar a Tela Home
            deviceListView.setVisibility(View.VISIBLE);
        }*/

        // Esconder a Barra de Ação
        try {
            getSupportActionBar().hide();
        } catch(Exception ignored) {}

        // Mostrar o bigDadLayout
        bigDadLayout.setVisibility(View.VISIBLE);

        verifyButton1();
        verifyButton2();
        verifyButton3();
        verifyFinishButton();
    }

    private void counter() {

        int qtd = 3;

        if (Ivan_Utils.getUser(ControlCenterv2.this).length > 2) {
            qtd --;
        }
        if (GB.isBluetoothEnabled()) {
            qtd --;
        }
        if (! deviceList.isEmpty()) {
            qtd --;
        }
        if (qtd == 0) {
            ((TextView) findViewById(R.id.stepsCountControlCenter)).setText(getString(R.string.no_more_steps));
        }
        else if (qtd == 1) {
            ((TextView) findViewById(R.id.stepsCountControlCenter)).setText(getString(R.string.one_more_step));
        }
        else if (qtd == 2) {
            ((TextView) findViewById(R.id.stepsCountControlCenter)).setText(getString(R.string.two_more_steps));
        }
        else if (qtd == 3) {
            ((TextView) findViewById(R.id.stepsCountControlCenter)).setText(getString(R.string.three_more_steps));
        }
    }

    private void checkButton(final int id) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ImageView img = (ImageView) findViewById(id);
                img.setColorFilter(greenColor);
                img.setImageResource(R.drawable.ic_confirm);
            }
        });
    }

    private void uncheckButton(final int id) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ImageView img = (ImageView) findViewById(id);
                img.setColorFilter(blueColor);
                img.setImageResource(R.drawable.right_arrow_blue);
            }
        });
    }

    private void asksForUserInfo() {
        //Preparing views
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.activity_formulario_ivan, (ViewGroup) findViewById(R.id.request_monitor_root_parent));
        //layout_root should be the name of the "top-level" layout node in the dialog_layout.xml file.

        final Spinner genderSp = (Spinner) layout.findViewById(R.id.sp_gender);
        final EditText nameEt = (EditText) layout.findViewById(R.id.et_name);
        final EditText ageEt = (EditText) layout.findViewById(R.id.et_age);

        String userData[] = Ivan_Utils.getUser(this);
        if (userData.length > 2) {
            nameEt.setText(userData[0]);
            ageEt.setText(userData[1]);

            if (userData[2].equalsIgnoreCase(getString(R.string.male_gender) + "\n") || userData[2].equalsIgnoreCase(getString(R.string.male_gender)))
                genderSp.setSelection(0);
            else
                genderSp.setSelection(1);
        }

        final Button okB = (Button) layout.findViewById(R.id.b_save_dt);
        //okB.setVisibility(View.GONE);

        //Initialize spinner
        String[] genders = new String[2];
        genders[0] = getString(R.string.male_gender);
        genders[1] = getString(R.string.female_gender);

        ArrayAdapter<String> spinnerArray = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, genders);
        spinnerArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSp.setAdapter(spinnerArray);
        genderSp.setSelection(0);

        //Building dialog
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder( this );
        builder.setTitle(getString(R.string.user_info));
        builder.setView(layout);
        builder.setCancelable(true);

        final android.app.AlertDialog dialog = builder.create();
        okB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String username = nameEt.getText().toString();
                String userAge = ageEt.getText().toString();
                String userGender = (String) genderSp.getSelectedItem();

                if (userAge.isEmpty())
                    ageEt.setError(getString(R.string.ivan_error_age));
                else if (Integer.parseInt(userAge) < 18 || Integer.parseInt(userAge) > 100)
                    ageEt.setError(getString(R.string.ivan_error_age));
                else {
                    if (username.isEmpty())
                        username = getString(R.string.user_info_not_informed);

                    Ivan_Utils.saveUserInformation(ControlCenterv2.this, username, userAge, userGender);

                    verifyButton1();
                    verifyFinishButton(); // Caso só faltasse as informações de usuário

                    dialog.dismiss();
                }
            }
        });
        dialog.show();

    }

    private boolean checkBluetoothAvailable() {
        BluetoothManager bluetoothService = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        if (bluetoothService == null) {
            //LOG.warn("No bluetooth available");
            this.adapter = null;
            return false;
        }
        BluetoothAdapter adapter = bluetoothService.getAdapter();
        if (adapter == null) {
            //LOG.warn("No bluetooth available");
            this.adapter = null;

            uncheckButton(R.id.linearImageOption1);
            return false;
        }
        if (!adapter.isEnabled()) {
            //LOG.warn("Bluetooth not enabled");
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            //Toast.makeText(this, "" + BluetoothAdapter.STATE_OFF, Toast.LENGTH_SHORT).show();
            startActivity(enableBtIntent);
            this.adapter = null;

            return false;
        }
        this.adapter = adapter;
        return true;
    }

    private void enableFinishButton() {
        Button finishButton = (Button) findViewById(R.id.controlCenterFinishButton);
        finishButton.setBackgroundResource(R.drawable.button_ivan_round);
        finishButton.setTextColor(Color.parseColor("#FFFFFF"));
        finishButton.setEnabled(true);
    }

    private void disableFinishButton() {
        Button finishButton = (Button) findViewById(R.id.controlCenterFinishButton);
        finishButton.setBackgroundResource(R.drawable.button_ivan_round_disabled);
        finishButton.setTextColor(Color.parseColor("#c1c1c1"));
        finishButton.setEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_menu_exit:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle(getString(R.string.app_name));
                alertDialog.setMessage(getString(R.string.ivan_confirm_exit));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(android.R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                GBApplication.quit();
                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(android.R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                return true;
            case R.id.home_menu_configure_server:
                startActivity(new Intent(this, SettingEnvironmentActivity.class));
                return true;
            default:
                return false;
        }
    }

    private void launchDiscoveryActivity() {
        startActivity(new Intent(this, DiscoveryActivity.class));
    }

    private void refreshPairedDevices() {

        List<GBDevice> deviceList = deviceManager.getDevices();

        /*
        if (Ivan_Utils.getStepsState(ControlCenterv2.this)) {
            try {
                getSupportActionBar().show();
            } catch(Exception ignored) {}
            bigDadLayout.setVisibility(View.GONE);
            deviceListView.setVisibility(View.VISIBLE);
        } else {
            try {
                getSupportActionBar().hide();
            } catch(Exception ignored) {}
            bigDadLayout.setVisibility(View.VISIBLE);
            deviceListView.setVisibility(View.GONE);
        }
        //

        if ( Ivan_Utils.getUser(this).length > 2) {
            checkButton(R.id.linearImageOption1);
        } else {
            uncheckButton(R.id.linearImageOption1);
        }

        if (GB.isBluetoothEnabled()) {
            checkButton(R.id.linearImageOption2);
        } else {
            uncheckButton(R.id.linearImageOption2);
        }

        if ( ! deviceList.isEmpty() ) {
            checkButton(R.id.linearImageOption3);

        } else {
            uncheckButton(R.id.linearImageOption3);
        }

        counter();

        if ( Ivan_Utils.getUser(this).length > 2 && GB.isBluetoothEnabled() && ! deviceList.isEmpty() ) {
            enableFinishButton();
        }
        else {
            disableFinishButton();
        }
        */
        stepsState = Ivan_Utils.getStepsState(ControlCenterv2.this);

        if (! stepsState) {
            verifyBigDadLayout();
            //Toast.makeText(this, "Mostra a Step Screen", Toast.LENGTH_SHORT).show();
        }
        else {
            checkBluetoothAvailable();
            //Toast.makeText(this, "Não Mostra a Step Screen", Toast.LENGTH_SHORT).show();

            try {
                getSupportActionBar().show();
            } catch(Exception ignored) {}

            deviceListView.setVisibility(View.VISIBLE);
        }

        mGBDeviceAdapter.notifyDataSetChanged();
    }
    @TargetApi(Build.VERSION_CODES.M)
    private void checkAndRequestPermissions() {
        List<String> wantedPermissions = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) == PackageManager.PERMISSION_DENIED)
            wantedPermissions.add(Manifest.permission.BLUETOOTH);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN) == PackageManager.PERMISSION_DENIED)
            wantedPermissions.add(Manifest.permission.BLUETOOTH_ADMIN);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_DENIED)
            wantedPermissions.add(Manifest.permission.READ_CONTACTS);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED)
            wantedPermissions.add(Manifest.permission.CALL_PHONE);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED)
            wantedPermissions.add(Manifest.permission.READ_PHONE_STATE);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.PROCESS_OUTGOING_CALLS) == PackageManager.PERMISSION_DENIED)
            wantedPermissions.add(Manifest.permission.PROCESS_OUTGOING_CALLS);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_DENIED)
            wantedPermissions.add(Manifest.permission.READ_SMS);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_DENIED)
            wantedPermissions.add(Manifest.permission.SEND_SMS);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
            wantedPermissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_DENIED)
            wantedPermissions.add(Manifest.permission.READ_CALENDAR);

        if (!wantedPermissions.isEmpty())
            ActivityCompat.requestPermissions(this, wantedPermissions.toArray(new String[wantedPermissions.size()]), 0);
    }

}
