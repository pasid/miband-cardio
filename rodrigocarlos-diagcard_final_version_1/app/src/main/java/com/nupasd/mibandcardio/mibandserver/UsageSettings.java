package com.nupasd.mibandcardio.mibandserver;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.nupasd.mibandcardio.R;
import com.nupasd.mibandcardio.activities.ControlCenterv2;

public class UsageSettings extends AppCompatActivity implements View.OnClickListener {

    private Button btnUsageSettingsOk;
    private Button btnUsageSettingsNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usage_settings);
        initializeComponents();
    }

    private void initializeComponents() {
        btnUsageSettingsOk = (Button) findViewById(R.id.btn_usage_settings_ok);
        btnUsageSettingsNo = (Button) findViewById(R.id.btn_usage_settings_no);

        btnUsageSettingsOk.setOnClickListener(this);
        btnUsageSettingsNo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_usage_settings_ok:
                Intent intent = new Intent(this, SettingEnvironmentActivity.class);
                intent.putExtra("firstUsage", true);
                startActivity(intent);
                finish();
                break;
            case R.id.btn_usage_settings_no:
                startActivity(new Intent(this, ControlCenterv2.class));
                finish();
                break;
        }
    }
}
