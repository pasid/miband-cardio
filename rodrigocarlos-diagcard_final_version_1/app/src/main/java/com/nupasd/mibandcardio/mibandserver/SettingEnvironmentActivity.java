package com.nupasd.mibandcardio.mibandserver;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nupasd.mibandcardio.R;
import com.nupasd.mibandcardio.activities.ControlCenterv2;

public class SettingEnvironmentActivity extends AppCompatActivity
        implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private TextView tvSettingInfo;
    private Button btnSettingConfigLater;

    private EditText edtSettingIp;
    private EditText edtSettingPort;
    private Button btnSettingSave;
    private Button btnSettingSync;

    private boolean control = false;
    private int intervalSelected = 2;

    private Spinner spnSettingInterval;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_environment);

        initializeComponents();
        fillFields();

        Intent intent = getIntent();
        if (!intent.getBooleanExtra("firstUsage", false)) {
            tvSettingInfo.setVisibility(View.INVISIBLE);
            btnSettingConfigLater.setVisibility(View.INVISIBLE);
            btnSettingSync.setVisibility(View.VISIBLE);
            control = true;
        }

    }

    private void fillFields() {
        SharedPreferences spf = getSharedPreferences("server_info", Context.MODE_PRIVATE);
        edtSettingIp.setText(spf.getString("ip", ""));
        edtSettingPort.setText(String.valueOf((spf.getInt("port", 0) == 0) ? "" : spf.getInt("port", 0)));
        if(spf.getInt("interval", 2) == 2){
            spnSettingInterval.setSelection(0);
        }else if(spf.getInt("interval", 2) == 1){
            spnSettingInterval.setSelection(1);
        }else{
            spnSettingInterval.setSelection(2);
        }
    }

    private void initializeComponents() {
        tvSettingInfo = (TextView) findViewById(R.id.tv_setting_info);
        btnSettingConfigLater = (Button) findViewById(R.id.btn_setting_config_later);

        edtSettingIp = (EditText) findViewById(R.id.edt_setting_ip);
        edtSettingPort = (EditText) findViewById(R.id.edt_setting_port);
        btnSettingSave = (Button) findViewById(R.id.btn_setting_save);
        btnSettingSync = (Button) findViewById(R.id.btn_setting_sync);

        spnSettingInterval = (Spinner) findViewById(R.id.spn_setting_interval);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.server_sync_interval_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSettingInterval.setAdapter(adapter);

        btnSettingConfigLater.setOnClickListener(this);
        btnSettingSave.setOnClickListener(this);
        spnSettingInterval.setOnItemSelectedListener(this);
        btnSettingSync.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_setting_config_later:
                startActivity(new Intent(this, ControlCenterv2.class));
                finish();
                break;
            case R.id.btn_setting_save:
                configureServer();
                break;
            case R.id.btn_setting_sync:
                switchSyncService();
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        changeStateBtnSync();
    }

    private void switchSyncService() {
        if(isMyServiceRunning(TimerSyncService.class)){
            stopService(new Intent(this, TimerSyncService.class));
            changeStateBtnSync();
        }else{
            startService(new Intent(this, TimerSyncService.class));
            changeStateBtnSync();
        }
    }

    private void changeStateBtnSync(){
        if(isMyServiceRunning(TimerSyncService.class)){
            btnSettingSync.setText("Synced");
            btnSettingSync.setBackgroundColor(Color.GREEN);
            btnSettingSync.setTextColor(Color.WHITE);

            edtSettingIp.setEnabled(false);
            edtSettingPort.setEnabled(false);
            spnSettingInterval.setEnabled(false);
            btnSettingSave.setEnabled(false);
        }else{
            btnSettingSync.setText("Synchronize");
            btnSettingSync.setBackgroundColor(Color.parseColor("#F67A70"));
            btnSettingSync.setTextColor(Color.WHITE);

            btnSettingSave.setEnabled(true);
            edtSettingIp.setEnabled(true);
            edtSettingPort.setEnabled(true);
            spnSettingInterval.setEnabled(true);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void configureServer() {
        //Salvar os dados do servidor com SharedPreferences
        SharedPreferences spf = getSharedPreferences("server_info", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = spf.edit();

        if (TextUtils.isEmpty(edtSettingIp.getText().toString())
                || TextUtils.isEmpty(edtSettingPort.getText().toString())) {
            Toast.makeText(this, "Digite as informações do servidor corretamente.", Toast.LENGTH_SHORT).show();
        } else {
            edit.clear();
            edit.putString("ip", edtSettingIp.getText().toString());
            edit.putInt("port", Integer.parseInt(edtSettingPort.getText().toString()));
            //0=segundos; 1=minutos; 2=horas.
            edit.putInt("interval", intervalSelected);
            edit.commit();

            if (control) {
                Toast.makeText(this, "Salvo.", Toast.LENGTH_SHORT).show();
            } else {
                startActivity(new Intent(this, ControlCenterv2.class));
                finish();
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String selected = adapterView.getItemAtPosition(i).toString().toUpperCase();
        switch (selected) {
            case "SECONDS":
                intervalSelected = 0;
                break;
            case "MINUTES":
                intervalSelected = 1;
                break;
            case "HOURS":
                intervalSelected = 2;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        intervalSelected = 2;
    }
}
