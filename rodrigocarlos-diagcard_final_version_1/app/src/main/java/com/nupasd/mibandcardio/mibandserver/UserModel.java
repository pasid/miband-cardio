package com.nupasd.mibandcardio.mibandserver;

import io.realm.RealmObject;

/**
 * Created by HILTON on 01/05/2018.
 */

public class UserModel extends RealmObject {
    public String name;
    public int age;
    public String sex;
}
